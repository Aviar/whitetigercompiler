[Git]: https://bitbucket.org/Aviar/whitetigercompiler.git
[GTest]: https://code.google.com/p/googletest/
[GMock]: https://code.google.com/p/googlemock/
[Doxygen]: http://www.doxygen.org/
[Boost]: http://www.boost.org/
[Bison]: http://www.gnu.org/software/bison/
[Flex]: http://flex.sourceforge.net/
[GCC]: http://gcc.gnu.org/
[Clang]: http://clang.llvm.org/
[VisualStudio]: http://www.visualstudio.com/
[CMake]: http://cmake.org

![](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/Mar/07/whitetigercompiler-logo-4135952268-3_avatar.png "White Tiger Compiler")

White Tiger is an augmented implementation of the Tiger Compiler found in "Modern
Compiler Implementation in C" by Andrew W. Appel. This compiler is not meant for 
production use, nor should it be used in any professional environment, it is purely 
__for educational purposes__.

- - -

## Dependencies ##
White Tiger requires [Bison][], [Flex][], [Boost][] and a C++11 compatible
compiler ([GCC 4.7+][GCC], [Visual Studio 2013][VisualStudio], or [Clang][]). [CMake][]
is required for generating the appropriate buildfiles.

Developers interested in building the tests will require [GTest][] and [GMock][],
as well as [Doxygen][] for generating documentation from source.

## Getting the Source ##
The WhiteTiger source may be downloaded from the Git repository located at the git 
[repo][Git]. To checkout using Git, open a terminal and execute:

	git clone https://bitbucket.org/Aviar/whitetigercompiler.git

If on Windows, Atlassians [SourceTree](https://www.atlassian.com/software/sourcetree)
is highly recommended.

## Generating the Build File ##
Run CMake in the projects root directory or open CMakeLists.txt in the CMake UI. To
generate the build files using CMake interactively on any OS run

	cmake -i

## Merging/Commiting ##
Please request write permissions from the repository owner. 

Only raw project source files should be commited to the repository. Be sure to have 
properly configured global .gitignore files to prevent polluting the trunk with
intermediate or software specific resources. Some useful .gitignore files may be 
found at the following URL:

	https://github.com/github/gitignore/ 

## License ##
![](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png "Creative Commons License")
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-nc-sa/3.0/ "License")


## Acknowledgments ##
Logo drawn by [Eagle-Flyte](http://eagle-flyte.deviantart.com/).
